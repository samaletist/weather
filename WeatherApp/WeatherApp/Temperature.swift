//
//  Temperature.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 14.05.21.
//

import Foundation
import RealmSwift

class Temperature:Object {
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var city: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var iconId: String = ""
    
    convenience init(temp: Double, country: String, city: String, iconId: String) {
        self.init()
        self.temp = temp
        self.city = city
        self.country = country
        self.iconId = iconId
    }
}
