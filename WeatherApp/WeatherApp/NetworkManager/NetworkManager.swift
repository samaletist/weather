//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 12.05.21.
//

import Foundation
import Moya
import Moya_ObjectMapper
import RealmSwift

final class NetworkManager {
    static let shared = NetworkManager()
    let provider = MoyaProvider<BackendAPI>(plugins: [NetworkLoggerPlugin()])
    private init() {}
    
    func getWeather(lat: Double, lon: Double, completion: @escaping (WheatherData) -> Void, failure: @escaping () -> Void) {
        provider.request(.getWeather(lat: lat, lon: lon)) {(result) in
            switch result {
            case let .success(response):
                guard let weather = try? response.mapObject(WheatherData.self) else {
                    failure()
                    return
                }
                completion(weather)
            case .failure(let error):
                failure()
            }
        }
    }
}
