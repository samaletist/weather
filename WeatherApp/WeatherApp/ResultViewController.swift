//
//  ResultViewController.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 14.05.21.
//

import UIKit
import RealmSwift

class ResultViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var savedData: [Temperature] = []
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        RealmManager.shared.readobject()
        readData()
    }
    
    func readData() {
        savedData = Array(realm.objects(Temperature.self))
        tableView.reloadData()
    }
    
    func registerCell() {
        let nib = UINib(nibName: String(describing: WeatherCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: WeatherCell.self))
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        RealmManager.shared.removeAll()
        readData()
    }
}

extension ResultViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeatherCell.self), for: indexPath)
        guard let weatherCell = cell as? WeatherCell else { return cell }
        weatherCell.setupWith(temperature: savedData[indexPath.row])
        return weatherCell
    }
}

extension ResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let item = savedData[indexPath.row]
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { [self] _, _, complete in
            self.savedData.remove(at: indexPath.row)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            RealmManager.shared.remove(temperature: item)
            complete(true)
        }
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}

