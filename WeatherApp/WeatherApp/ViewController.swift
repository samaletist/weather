//
//  ViewController.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 12.05.21.
//

import UIKit
import GoogleMaps
import RealmSwift

class ViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var downloadIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    
    @IBAction func resultAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ResultViewController") as? ResultViewController else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        downloadIndicator.startAnimating()
        NetworkManager.shared.getWeather(lat: coordinate.latitude, lon: coordinate.longitude) { weatherData in
            guard let temp = weatherData.main?.temp else {return}
            guard let place = weatherData.system?.country else { return }
            guard let iconId = weatherData.weather?[0].icon else {return}
            let alert = UIAlertController(title: "Погода", message: "Температура - \(temp)°С. Страна - \(place). Город - \(weatherData.name)", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Закрыть" , style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true)
            let city = weatherData.name
            let temperature = Temperature(temp: temp, country: place, city: city, iconId: iconId)
            let realmObject = try! Realm()
            try! realmObject.write {
                realmObject.add(temperature)
                self.downloadIndicator.stopAnimating()
            }
        } failure: {
            self.downloadIndicator.stopAnimating()
        }
    }
}
