//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 14.05.21.
//

import UIKit
import SDWebImage

class WeatherCell: UITableViewCell {
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupWith(temperature: Temperature) {
        tempLabel.text = "Температура: \(temperature.temp) °С"
        cityLabel.text = "Город: \(temperature.city)"
        countryLabel.text = "Страна: \(temperature.country)"
        let urlStr = "https://openweathermap.org/img/wn/\(temperature.iconId)@2x.png"
        self.weatherImage.sd_setImage(with: URL(string:urlStr), placeholderImage: UIImage(named: "placeholder.png"))
    }
}

