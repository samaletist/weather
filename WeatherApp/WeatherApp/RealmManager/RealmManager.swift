//
//  RalmManager.swift
//  WeatherApp
//
//  Created by Melnik Anatol on 14.05.21.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    let realm = try! Realm()
    
    private init () {}
    
    func removeAll() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func remove(temperature: Temperature) {
        try! realm.write {
            realm.delete(temperature)
        }
    }
    
    func readobject() -> [Temperature] {
        return Array(realm.objects(Temperature.self))
    }
}
